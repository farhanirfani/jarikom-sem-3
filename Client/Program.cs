﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Threading;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpClient Client = new TcpClient("127.0.0.1", 1200);
            NetworkStream read = Client.GetStream();
            Console.WriteLine("[connected]");

            Console.WriteLine("Connected to server.");
            Console.WriteLine("");
            Console.WriteLine("============PETUNJUK PERMAINAN===========");
            Console.WriteLine("Masukkan Huruf");
            Console.WriteLine("Server akan mengirim 4 huruf setelah huruf");
            Console.WriteLine("yang Anda input");
            Console.WriteLine("============SELAMAT BERMAIN===========");

            string kk;

            Console.Write("Input : ");

            while ((kk = Console.ReadLine()) != "0")
            {
                // mengirim
                byte[] message = Encoding.Unicode.GetBytes(kk);
                read.Write(message, 0, message.Length);


                //menerima
                byte[] bytesToRead = new byte[Client.ReceiveBufferSize];
                int data = read.Read(bytesToRead, 0, Client.ReceiveBufferSize);

                Console.Write("Hasil : ");
                Console.Write(Encoding.Unicode.GetString(bytesToRead, 0, data));

                Console.WriteLine("");

                Console.Write(">>");
            }

            Client.Close();
            Console.ReadKey();

        }
    }
}
