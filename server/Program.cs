﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Threading;

namespace TCP1415Server
{
    class Program
    {
        private static TcpListener tcpListener;
        static void Main(string[] args)
        {
            int k = 30;

            tcpListener = new TcpListener(IPAddress.Parse("127.0.0.1"), 1200);

            Console.WriteLine("[Listening...]");
            tcpListener.Start();
            TcpClient client = tcpListener.AcceptTcpClient();   
            Console.WriteLine("[Client connected]");
            while (true)
            {
                NetworkStream stream = client.GetStream(); 
                byte[] buffer = new byte[client.ReceiveBufferSize];
                int data = stream.Read(buffer, 0, client.ReceiveBufferSize); 
                string ch = Encoding.Unicode.GetString(buffer, 0, data);    
                Console.WriteLine("Message received : " + ch);
                ch = convertString(ch, k);

                Console.WriteLine("Converting...");
                Console.WriteLine("Sending data" + ch);

                byte[] message = Encoding.Unicode.GetBytes(ch);
                stream.Write(message, 0, message.Length);


            }

            client.Close();
            Console.ReadKey();


        }

        static string convertString(string s, int k)
        {
 
            String newS = "";


            for (int i = 0; i < s.Length; ++i)
            {
 
                int val = s[i]; 
                int dup = k;


              

                if (val + k > 122)
                {
                    k -= (122 - val);
                    k = k % 26;
                    if (k == 0)
                        k += 26;
                    newS += (char)(96 + k);
                }
                else if (val == 32)
                {
                    newS += (char)(val);
                }
                else
                {
                    newS += (char)(96 + k);
                }

                k = dup;
            }

            
            return (newS);


        }
    }
}
